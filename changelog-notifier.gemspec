# frozen_string_literal: true

require_relative 'lib/changelog/notifier/version'

Gem::Specification.new do |spec|
  spec.name          = 'changelog-notifier'
  spec.version       = Changelog::Notifier::VERSION
  spec.authors       = ['Guillaume Hain']
  spec.email         = ['zedtux@zedroot.org']

  spec.summary       = "Sends current version's release note from your " \
                       'CHANGELOG.md file'
  spec.description   = 'This gem sends the release note, extracted from your ' \
                       'CHANGELOG.md file, to Slack when deployin with ' \
                       'Capistrano'
  spec.homepage      = 'https://gitlab.com/zedtux/changelog-notifier'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir['lib/**/*'] + %w[README.md LICENSE CHANGELOG.md]
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'titleize', '~> 1.4.1'
end
