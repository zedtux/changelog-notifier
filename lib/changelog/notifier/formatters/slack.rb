# frozen_string_literal: true

require 'date'
require 'titleize'

module Changelog
  module Notifier
    module Formatters
      #
      # Format the given release note hash for Slack
      #
      class Slack
        def initialize(release_note_hash)
          @release_note_hash = release_note_hash
        end

        def format
          {
            attachments: build_attachments,
            text: description_text
          }
        end

        private

        def build_attachments
          Array(@release_note_hash[:changes]).map do |change, logs|
            {
              mrkdwn_in: ['text'],
              color: color_for(change),
              title: change.to_s.capitalize,
              fields: logs.map { |log| { title: log } }
            }
          end
        end

        def color_for(change)
          case change
          when :added       then green
          when :changed     then yellow
          when :deprecated  then yellow
          when :fixed       then green
          when :removed     then red
          when :security    then red
          end
        end

        def description_text
          "*#{formatted_application_name}* version " \
          "*#{@release_note_hash[:version]}* has just been released by " \
          "*#{@release_note_hash[:author]}* on the " \
          "*#{formatted_release_date}*.\n\nPlease find bellow the release note:"
        end

        def formatted_application_name
          @release_note_hash[:application].gsub(/_/, ' ').titleize
        end

        def formatted_release_date
          Date.parse(@release_note_hash[:date]).strftime('%b %d')
        end

        def green
          '#36a64f'
        end

        def red
          '#fc464a'
        end

        def yellow
          '#fd9134'
        end
      end
    end
  end
end
