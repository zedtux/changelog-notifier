# frozen_string_literal: true

namespace :load do
  task :defaults do
    set :changelog_notifier_role, :app

    # Slack
    set :changelog_notifier_slack_webhook_url, nil
    set :changelog_notifier_slack_channel, '#general'
    set :changelog_notifier_slack_icon_emoji, ':package:'

    # ActiveRecord
    set :changelog_notifier_active_record_model, nil # nil means disabled
    set :changelog_notifier_active_record_version_field, 'version'
    set :changelog_notifier_active_record_release_node_field, 'release_note'
    set :changelog_notifier_active_record_other_fields, {}
  end
end

namespace :deploy do
  after :deploy, 'changelog_notifier:post_release_note'
end

namespace :changelog_notifier do
  desc "Posts version's release notee do Slack"
  task :post_release_note do
    on roles(fetch(:changelog_notifier_role)) do
      within current_path do
        Changelog::Notifier::Entrypoints::Capistrano.run!(self)
      end
    end
  end
end
