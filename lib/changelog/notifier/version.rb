# frozen_string_literal: true

module Changelog
  module Notifier
    VERSION = '1.5.2'
  end
end
