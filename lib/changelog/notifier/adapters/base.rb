# frozen_string_literal: true

module Changelog
  module Notifier
    module Adapters
      class Base
        def initialize(capistrano)
          @capistrano = capistrano

          unless @capistrano
            raise ArgumentError, 'No Capistrano instance passed while it is ' \
                                 'required.'
          end

          fetches_adapter_configuration
        end

        def configured?
          raise NotImplementedError
        end

        def publish!(release_note_hash, version)
          raise NotImplementedError
        end
      end
    end
  end
end
