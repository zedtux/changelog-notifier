# frozen_string_literal: true

module Changelog
  module Notifier
    # The CHANGELOG doesn't include the given version
    class ReleaseNoteNotFound < StandardError; end
    # A git command failed with an exit code higher than 0
    class GitCommandFailed < StandardError; end

    # A class is not implementing an expected method
    class NotImplementedError < StandardError; end
  end
end
