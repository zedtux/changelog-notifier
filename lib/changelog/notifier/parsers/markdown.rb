# frozen_string_literal: true

module Changelog
  module Notifier
    module Parsers
      #
      # Parses the given `content` as Markdown following the specifications from
      # https://keepachangelog.com.
      #
      class Markdown
        def initialize(content)
          @content = content
        end

        def extract(version)
          valid_version?(version) || raise(ArgumentError, 'Invalid version')

          parse!

          version_release_note = detect_version_release_note_for(version)
          version_release_note || raise(Changelog::Notifier::ReleaseNoteNotFound)

          create_hash_from(version_release_note).tap do |hash|
            hash[:url] = fetch_version_url_for(version)
          end
        end

        private

        # See https://keepachangelog.com/en/1.0.0/#how
        def change_types
          %w[
            added
            changed
            deprecated
            removed
            fixed
            security
          ]
        end

        def create_hash_from(version_release_note)
          sections = version_release_note.split('### ')

          # Removes trailing whitespaces
          sections = sections.map { |section| section.gsub(/\s+$/, '') }

          version, date = extract_version_and_date_from(sections.shift)

          changes = extract_actions_from(sections)

          { version: version, date: date, changes: changes }
        end

        def detect_version_release_note_for(version)
          version_number = fetch_version_number_from(version)

          @versions.detect do |changelog_version|
            changelog_version.start_with?("#{version_number}]") ||
              changelog_version.start_with?("#{version}]")
          end
        end

        def extract_actions_from(sections)
          sections.each_with_object({}) do |section, accumulator|
            type, logs = section.match(/^(#{change_types.join('|')})\n(.*)/mi)
                               &.captures

            next unless type

            type = type.downcase.to_sym

            logs = logs.split(/\s+-/)
                       .reject(&:empty?)
                       .map { |log| log.gsub(/^\s+/, '') }
                       .map { |log| log.gsub("\n", ' ') }

            accumulator[type] = logs
          end
        end

        def extract_version_and_date_from(section)
          section.match(/([\d\.]+)\]\s+-\s+([\d+-]+)/)&.captures
        end

        def fetch_version_number_from(version)
          version.match(/([\d\.]+)/)&.captures&.first
        end

        def fetch_version_url_for(version)
          version_number = fetch_version_number_from(version)
          @content.match(%r{\[#{version_number}\]: (https://[\S]+)})
                 &.captures
                 &.first
        end

        #
        # Split the file on the double dashes, representing version sections.
        #
        def parse!
          @versions = @content.split(/##\s\[/)
        end

        def valid_version?(version)
          version =~ /[\d\.]+/
        end
      end
    end
  end
end
