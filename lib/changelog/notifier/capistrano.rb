# frozen_string_literal: true

require 'changelog/notifier/entrypoints/capistrano'
require 'changelog/notifier/git_tag_fetcher'
require 'changelog/notifier/git_commit_author_fetcher'
require 'changelog/notifier/errors'
require 'changelog/notifier/parsers/markdown'
require 'changelog/notifier/adapters/slack'
require 'changelog/notifier/formatters/slack'

load File.expand_path('capistrano/tasks/changelog_notifier.rake', __dir__)
