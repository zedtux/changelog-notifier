# frozen_string_literal: true

require 'open3'

module Changelog
  module Notifier
    #
    # Fetches a Git commit author from the current commit.
    #
    class GitCommitAuthorFetcher
      def self.fetch(options = {})
        output = options[:capistrano].capture(
          "git --git-dir=#{options[:path] || '.'} log -1 --pretty=format:'%an'"
        )

        return nil if output.empty?

        output.gsub(/(^\'|'$)/, '')
      end
    end
  end
end
