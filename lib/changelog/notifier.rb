# frozen_string_literal: true

require 'changelog/notifier/errors'
require 'changelog/notifier/adapters/base'
require 'changelog/notifier/adapters/active_record'
require 'changelog/notifier/adapters/slack'
require 'changelog/notifier/formatters/active_record'
require 'changelog/notifier/formatters/slack'
require 'changelog/notifier/git_commit_author_fetcher'
require 'changelog/notifier/git_tag_fetcher'
require 'changelog/notifier/parsers/markdown'
require 'changelog/notifier/version'

module Changelog
  module Notifier
  end
end
