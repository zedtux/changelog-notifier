# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.5.2] - 2023-05-04
### Changed
- Switches to Earthly
- Reworks the code in order to prevent passing the capistrano intance to the
  Markdown parser

### Fixes
- Typos in sentences from code and tests

## [1.5.1] - 2021-05-25
### Fixed
- Prevents calling the `publish!` adapter's method with unconfigured adapters

## [1.5.0] - 2020-10-18
### Added
- ActiveRecord adapter

## [1.4.0] - 2020-10-13
### Added
- Screenshot in the `README.md` file

### Removed
- Remaning debugs

## [1.3.0] - 2020-10-13
### Fixed
- Running commands remotely

## [1.2.0] - 2020-10-13
### Fixed
- Using git on a Capistrano server

## [1.1.0] - 2020-10-13
### Added
- Improves application name formatting in Slack adapter

## [1.0.0] - 2020-10-12
### Added
- Capistrano entrypoint
- CHANGELOG.md parser based on http://keepachangelog.com
- Version fetching based on git tags
- Version commit author fetching
- Slack adapter to post the changelog to a Slack channel
- Slack formatter

[Unreleased]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.5.2...master
[1.5.2]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.5.1...v1.5.2
[1.5.1]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.5.0...v1.5.1
[1.5.0]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.4.0...v1.5.0
[1.4.0]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.3.0...v1.4.0
[1.3.0]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.1.0...v1.2.0
[1.1.0]: https://gitlab.com/zedtux/changelog-notifier/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/zedtux/changelog-notifier/-/tags/v1.0.0
