# changelog-notifier

This gem reads [your `CHANGELOG.md` file](https://keepachangelog.com) and extract the release note for your project's version (fetched from your git tags) and posts it to a Slack channel when deploying your app with Capistrano.

This allows you to keep informed your team about releases deployments.

To summarize, it runs your `CHANGELOG.md` file into this:

![](screenshots/changelog-notifiier_slack.png)

## Concepts

### Entrypoints

[Entrypoints](https://gitlab.com/zedtux/changelog-notifier/-/tree/master/lib/changelog/notifier/entrypoints) are the inputs, or the triggers. As of now, only the Capistrano entrypoint exists, which means you can only publish your `CHANGELOG.md` *FROM* a Capistrano deployment.

You can add any kind of entrypoints like a cron one, a bot and more!

### Parsers

[Parsers](https://gitlab.com/zedtux/changelog-notifier/-/tree/master/lib/changelog/notifier/parsers) are the heart of this gem, and are responsible to read and transform a `CHANGELOG.md` file into a standardized `Hash`.

In other words they are transforming something like that:

```
## [1.1.0] - 2020-10-13
### Added
- Improves application name formatting in Slack adapter

### Removed
- All the bad code
```

Into that:

```ruby
{
    version: '1.1.0',
    date: '2020-10-13',
    url: 'https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...v1.1.0',
    changes: {
      added: [
        'Improves application name formatting in Slack adapter',
      ],
      removed: [
        'All the bad code'
      ]
    }
}
```

### Adapters

[Adapters](https://gitlab.com/zedtux/changelog-notifier/-/tree/master/lib/changelog/notifier/adapters) are the output. Adapters are using [formatters](https://gitlab.com/zedtux/changelog-notifier/-/tree/master/lib/changelog/notifier/formatters) in order to transform the parsed `CHANGELOG.md` into something that can be used by the adapters.

As of now, there are 2 supported adapters:

* the Slack adapter which allows you to publish your `CHANGELOG.md` *TO* a Slack channel
* the ActiveRecord which allows you to create an ActiveRecord into your database

You can use many adapters at the same time so if you'd like to post to Slack *AND* to create an entry in your database, configures both and you'll get both!

You can add adapters for anythings like Twitter, Mails, SMS, WhatsApp and more!

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'changelog-notifier', '~> 1.4'
gem 'slack-notifier', '~> 2' # For Slack notifications
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install changelog-notifier

## Usage

### First

As of now there's only one [Entrypoint](#entrypoints) which is Capistrano, so you have to import the gem's Capistrano entrypoint:

1. Add the following `require` to your `Capfile`
```ruby
require 'changelog/notifier/capistrano'
```
2. Add and configure the options in your `config/deploy.rb` file:
```ruby
# Where should changelog-notifier run?
set :changelog_notifier_role, :app
```

### Using the Slack adapter

3. Configure the Slack adapter:
```ruby
# Slack options
#
# Your webhook URL
set :changelog_notifier_slack_webhook_url, 'https://hooks.slack.com/services/...'
# The Slack channel where to post the release note
set :changelog_notifier_slack_channel, '#general'
# The Icon Emoji to be used when posting the release note
set :changelog_notifier_slack_icon_emoji, ':package:'
```

### Using the ActiveRecord adapter

3. Configure the ActiveRecord adapter:
```ruby
# ActiveRecord options
#
# Your ActiveRecord model name
set :changelog_notifier_active_record_model, Release
# The column/field from your ActiveRecord model where to set the version
set :changelog_notifier_active_record_version_field, 'version'
# The column/field from your ActiveRecord model where to store the release note
set :changelog_notifier_active_record_release_node_field, 'release_note'
# The columns/fields from your ActiveRecord model that you need to set (optional)
set :changelog_notifier_active_record_other_fields, { field1: 'ok', field2: true }
```

### Finally

3. Maintain your `CHANGELOG.md` file as described at https://keepachangelog.com
4. Tag with your version
5. Deploy with Capistrano

When you will deploy a tagged commit, and a matching version exist in your
`CHANGELOG.md` file, then a Slack post will be sent on a successful Capistrano
deployment.

## Development

This section describe how to use Docker to develop this gem, but you can also
develop the "old way" by installing Ruby and all the depedencies on you machine.

You need to install:
1. [Docker](https://docs.docker.com/get-docker/)
2. [Earthly](https://earthly.dev/get-earthly) (`Makefile` + `Dockerfile` made
   easy, parallel and isolated)

After checking out the repo, you can build the Docker image using Earthly by
running the command `earthly +dev`.

Now you have 2 options:
* Run the tests using `docker-compose`
```
docker-compose run --rm gem
```
* Run the tests using Earthly (isolated, like on the CI, mitigating the
  differences between your local machine and the CI, preventing then tests
  passing on your local machine, but not on the CI)
```
earthly --allow-privileged +rspec
```

## Publish a new version

1. Update the `CHANGELOG.md` file in order to move all the updates from the
   `Unlreased` section to a new version (Don't forget to update the version link
   from the bottom of the `CHANGELOG.md` file)
2. Update the `lib/changelog/notifier/version.rb` file with the same version
   number
3. Commit, tag the commit, and push
4. Publish the new version using Earthly (Update the `RUBYGEMS_OTP`):
```
earthly +gem --GEM_CREDENTIALS="$(cat ~/.gem/credentials)" --RUBYGEMS_OTP=123456
```

## Contributing

Bug reports and merge requests are welcome on Gitlab at https://gitlab.com/zedtux/changelog-notifier.
This project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org)
code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Changelog::Notifier project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/zedtux/changelog-notifier/blob/master/CODE_OF_CONDUCT.md).
