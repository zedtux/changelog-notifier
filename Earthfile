VERSION 0.6

# This allows one to change the running Ruby version with:
#
# `earthly --build-arg EARTHLY_RUBY_VERSION=3 --allow-privileged +rspec`
ARG EARTHLY_RUBY_VERSION=2.7

FROM ruby:$EARTHLY_RUBY_VERSION-alpine
WORKDIR /gem

deps:
    ARG DEV_PACKAGES="c-ares git shared-mime-info sqlite-dev"
    ARG PACKAGES="tzdata"

    RUN apk add --no-cache --update $DEV_PACKAGES \
                                    $PACKAGES \
        && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
        && echo $TZ > /etc/timezone \
        # Add a group or add a user to a group
        #
        #  -g GID   Group id
        #  -S       Create a system group
        #
        # Create new user, or add USER to GROUP
        #
        #  -h DIR   Home directory
        #  -g GECOS GECOS field
        #  -s SHELL Login shell
        #  -G GRP   Group
        #  -S       Create a system user
        #  -D       Don't assign a password
        #  -H       Don't create home directory
        #  -u UID   User id
        #  -k SKEL  Skeleton directory (/etc/skel)
        && addgroup -S changelog-notifier \
        && adduser -s /bin/sh \
                   -G changelog-notifier \
                   -S \
                   -u 1000 \
                   changelog-notifier \
        && chown -R changelog-notifier:changelog-notifier /gem

gems:
    FROM +deps

    RUN apk add --no-cache --update build-base \
        && touch ~/.gemrc \
        && echo "gem: --no-ri --no-rdoc" >> ~/.gemrc \
        && gem install rubygems-update \
        && update_rubygems

    ARG BUNDLER_VERSION=2.1.4

    RUN gem install bundler:${BUNDLER_VERSION}

    COPY Gemfile* /gem
    COPY *.gemspec /gem
    COPY lib/changelog/notifier/version.rb /gem/lib/changelog/notifier/

    RUN bundle install --jobs $(nproc)

    SAVE ARTIFACT /usr/local/bundle bundle

dev:
    FROM +deps

    COPY +gems/bundle /usr/local/bundle

    COPY .rspec /gem

    COPY Gemfile* /gem
    COPY *.gemspec /gem

    COPY Rakefile /gem

    COPY lib/ /gem/lib/
    COPY spec/ /gem/spec/

    RUN chown -R changelog-notifier:changelog-notifier /gem

    USER changelog-notifier

    ENTRYPOINT ["bundle", "exec"]
    CMD ["rake"]

    ARG TAG=latest
    SAVE IMAGE --push registry.gitlab.com/zedtux/changelog-notifier:$TAG

#
# This target runs the test suite.
#
# Use the following command in order to run the tests suite:
# earthly --allow-privileged +rspec
rspec:
    FROM earthly/dind:alpine

    COPY docker-compose*.yml ./

    WITH DOCKER --load registry.gitlab.com/zedtux/changelog-notifier:latest=+dev
        RUN docker-compose -f docker-compose-earthly.yml run --rm gem
    END

#
# This target is used to publish this gem to rubygems.org.
#
# Prerequiries
# You should have login against Rubygems.org so that it has created
# the `~/.gem` folder and stored your API key.
#
# Then use the following command:
# earthly +gem --GEM_CREDENTIALS="$(cat ~/.gem/credentials)" --RUBYGEMS_OTP=123456
gem:
    FROM +dev

    ARG GEM_CREDENTIALS
    ARG RUBYGEMS_OTP

    COPY .git/ /gem/
    COPY CHANGELOG.md /gem/
    COPY LICENSE /gem/
    COPY README.md /gem/

    RUN gem build changelog-notifier.gemspec \
        && mkdir ~/.gem \
        && echo "$GEM_CREDENTIALS" > ~/.gem/credentials \
        && cat ~/.gem/credentials \
        && chmod 600 ~/.gem/credentials \
        && gem push --otp $RUBYGEMS_OTP changelog-notifier-*.gem

    SAVE ARTIFACT changelog-notifier-*.gem AS LOCAL ./changelog-notifier.gem
