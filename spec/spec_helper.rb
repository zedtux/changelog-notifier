# frozen_string_literal: true

require 'bundler/setup'
require 'changelog/notifier'
require 'webmock/rspec'

require 'active_record'
require 'database_cleaner'

require 'support/fake_capistrano'
require 'support/git_helpers'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.include GitHelpers

  config.before(:suite) do
    # Imports the ActiveRecord config file
    require 'support/active_record_setup'

    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
