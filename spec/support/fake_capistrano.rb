# frozen_string_literal: true

require 'open3'

class FakeCapistrano
  def error(message)
    puts "[FakeCapistrano] ERROR: #{message}"
  end

  def fetch(*); end

  def info(message)
    puts "[FakeCapistrano] INFO: #{message}"
  end

  def capture(command)
    out, st = Open3.capture2(command)

    unless st.exitstatus.zero?
      puts "WARNING: Command `#{command}` ended with exit code " \
           "#{st.exitstatus}."
    end

    out
  end

  def warn(message)
    puts "[FakeCapistrano] WARNING: #{message}"
  end
end
