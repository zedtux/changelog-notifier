# frozen_string_literal: true

ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: ':memory:'
)

ActiveRecord::Schema.define do
  self.verbose = false

  create_table :info_versions, force: true do |t|
    t.string :version
    t.string :release_node
    t.boolean :refresh

    t.timestamps
  end
end
