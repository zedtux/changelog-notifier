# frozen_string_literal: true

RSpec.describe Changelog::Notifier::GitTagFetcher do
  describe '#fetch' do
    context 'without being in a Git repository' do
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: '/tmp'
        )
      end

      it 'returns nil' do
        expect(result).to be_nil
      end
    end

    context 'when in a Git repository but with a commit without any tags' do
      before { clean_remote_git_repo('fake') }

      let(:repo) { remote_git_repo('fake') }
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: repo
        )
      end

      it 'returns nil' do
        expect(result).to be_nil
      end
    end

    context 'when in a Git repository with a tagged commit but not for ' \
            'a version' do
      before { clean_remote_git_repo('fake') }

      let(:repo) { remote_git_repo('fake', annotated_tags: ['abcd']) }
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: repo
        )
      end

      it 'returns nil' do
        expect(result).to be_nil
      end
    end

    context 'when in a Git repository with a tagged commit for a version' do
      before { clean_remote_git_repo('fake') }

      let(:repo) { remote_git_repo('fake', annotated_tags: ['v1.0.0']) }
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: repo
        )
      end

      it 'returns nil' do
        expect(result).to eql 'v1.0.0'
      end
    end

    context 'when in a Git repository with multiple tagged commit including ' \
            'one for a version' do
      before { clean_remote_git_repo('fake') }

      let(:repo) do
        remote_git_repo('fake', annotated_tags: ['abcd', 'v1.0.0', 'defg'])
      end
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: repo
        )
      end

      it 'returns nil' do
        expect(result).to eql 'v1.0.0'
      end
    end
  end
end
