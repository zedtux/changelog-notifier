# frozen_string_literal: true

RSpec.describe Changelog::Notifier::Formatters::Slack do
  describe '#format' do
    context 'with a full release note hash' do
      let(:release_note_hash) do
        {
          application: 'twitter',
          version: '1.0.0',
          date: '2017-06-20',
          url: nil,
          author: 'Guillaume Hain',
          changes: {
            added: [
              'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
              'Version navigation.',
              'Links to latest released version in previous versions.'
            ],
            changed: [
              "Start using \"changelog\" over \"change log\" since it's the " \
              'common usage.',
              'Start versioning based on the current English version at ' \
              '0.3.0 to help translation authors keep things up-to-date.'
            ],
            removed: [
              'Section about "changelog" vs "CHANGELOG".'
            ]
          }
        }
      end

      it 'formats it for Slack' do
        expect(described_class.new(release_note_hash).format).to eql(
          {
            text: '*Twitter* version *1.0.0* has just been released by ' \
                  '*Guillaume Hain* on the *Jun 20*.' \
                  "\n\nPlease find bellow the release note:",
            attachments: [
              {
                mrkdwn_in: ['text'],
                color: '#36a64f',
                title: 'Added',
                fields: [
                  {
                    title: 'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).'
                  },
                  {
                    title: 'Version navigation.'
                  },
                  {
                    title: 'Links to latest released version in previous versions.'
                  }
                ]
              },
              {
                mrkdwn_in: ['text'],
                color: '#fd9134',
                title: 'Changed',
                fields: [
                  {
                    title: 'Start using "changelog" over ' \
                           "\"change log\" since it's the common usage."
                  },
                  {
                    title: 'Start versioning based on the current ' \
                           'English version at 0.3.0 to help translation ' \
                           'authors keep things up-to-date.'
                  }
                ]
              },
              {
                mrkdwn_in: ['text'],
                color: '#fc464a',
                title: 'Removed',
                fields: [
                  {
                    title: 'Section about "changelog" vs "CHANGELOG".'
                  }
                ]
              }
            ]
          }
        )
      end
    end
  end

  describe 'application name formatting' do
    context 'with a multi words application name' do
      let(:release_note_hash) do
        {
          application: 'my_beautiful_application',
          date: '2017-06-20'
        }
      end

      it 'returns a spaces and capitalized version' do
        release_note = described_class.new(release_note_hash).format
        expect(release_note[:text]).to start_with('*My Beautiful Application*')
      end
    end
  end
end
