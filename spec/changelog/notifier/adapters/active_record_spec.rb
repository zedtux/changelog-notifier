# frozen_string_literal: true

class InfoVersion < ::ActiveRecord::Base; end

RSpec.describe Changelog::Notifier::Adapters::ActiveRecord do
  let(:capistrano) { FakeCapistrano.new }

  context 'when not passing any ActiveRecord model' do
    before do
      # ActiveRecord model
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_active_record_model)
        .and_return(nil)

      # ActiveRecord other variables
      allow(capistrano)
        .to receive(:fetch)
        .and_return(nil)
    end

    it "doesn't call the publish! method" do
      expect(described_class.new(capistrano)).not_to receive(:publish!)
    end
  end

  context 'when passing an ActiveRecord model' do
    let(:version) { '1.0.0' }
    let(:release_note_hash) do
      {
        application: 'twitter',
        version: version,
        date: '2017-06-20',
        url: nil,
        author: 'Guillaume Hain',
        changes: {
          added: [
            'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
            'Version navigation.',
            'Links to latest released version in previous versions.'
          ],
          changed: [
            "Start using \"changelog\" over \"change log\" since it's the " \
            'common usage.',
            'Start versioning based on the current English version at ' \
            '0.3.0 to help translation authors keep things up-to-date.'
          ],
          removed: [
            'Section about "changelog" vs "CHANGELOG".'
          ]
        }
      }
    end

    before do
      # Other ActiveRecord fields
      allow(capistrano)
        .to receive(:fetch)
        .and_return(nil)

      # ActiveRecord model
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_active_record_model)
        .and_return(InfoVersion)

      described_class.new(capistrano).publish!(release_note_hash, version)
    end

    it 'creates an InfoVersion record' do
      expect(InfoVersion.count).to be 1
    end

    it 'creates an InfoVersion record with the version column defined' do
      expect(InfoVersion.first.version).to eql(version)
    end

    it 'creates an InfoVersion record with the version column defined' do
      expect(InfoVersion.first.release_node).to eql(
        <<~LOG
          *Twitter* version *1.0.0* has just been released by *Guillaume Hain* on the *Jun 20*.
          Please find bellow the release note:

          Added
          New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).
          Version navigation.
          Links to latest released version in previous versions.

          Changed
          Start using "changelog" over "change log" since it's the common usage.
          Start versioning based on the current English version at 0.3.0 to help translation authors keep things up-to-date.

          Removed
          Section about "changelog" vs "CHANGELOG".
        LOG
      )
    end
  end

  context 'when passing an ActiveRecord model and other fields' do
    let(:version) { '1.0.0' }
    let(:release_note_hash) do
      {
        application: 'twitter',
        version: version,
        date: '2017-06-20',
        url: nil,
        author: 'Guillaume Hain',
        changes: {
          added: [
            'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
            'Version navigation.',
            'Links to latest released version in previous versions.'
          ],
          changed: [
            "Start using \"changelog\" over \"change log\" since it's the " \
            'common usage.',
            'Start versioning based on the current English version at ' \
            '0.3.0 to help translation authors keep things up-to-date.'
          ],
          removed: [
            'Section about "changelog" vs "CHANGELOG".'
          ]
        }
      }
    end

    before do
      # Other ActiveRecord fields
      allow(capistrano)
        .to receive(:fetch)
        .and_return(nil)

      # ActiveRecord model
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_active_record_model)
        .and_return(InfoVersion)

      # ActiveRecord other_fields
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_active_record_other_fields)
        .and_return({ refresh: true })

      described_class.new(capistrano).publish!(release_note_hash, version)
    end

    it 'creates an InfoVersion record' do
      expect(InfoVersion.count).to be 1
    end

    it 'creates an InfoVersion record with the other column defined' do
      expect(InfoVersion.first.refresh).to be_truthy
    end
  end
end
