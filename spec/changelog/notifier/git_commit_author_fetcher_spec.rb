# frozen_string_literal: true

RSpec.describe Changelog::Notifier::GitCommitAuthorFetcher do
  describe '#fetch' do
    context 'without being in a Git repository' do
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: '/tmp'
        )
      end

      it 'returns nil' do
        expect(result).to be_nil
      end
    end

    context 'when in a Git repository' do
      before { clean_remote_git_repo('fake') }

      let(:repo) { remote_git_repo('fake') }
      let(:result) do
        described_class.fetch(
          capistrano: FakeCapistrano.new,
          path: repo
        )
      end

      it 'returns omnibus' do
        expect(result).to eql('omnibus')
      end
    end
  end
end
