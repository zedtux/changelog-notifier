# frozen_string_literal: true

RSpec.describe Changelog::Notifier do
  it 'has a version number' do
    expect(Changelog::Notifier::VERSION).not_to be nil
  end
end
